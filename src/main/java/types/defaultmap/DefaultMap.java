package types.defaultmap;

import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * Equivalent of <a
 * href=https://docs.python.org/3/library/collections.html#collections.defaultdict>defaultdict</a>
 * in the Python collections library.
 */
@EqualsAndHashCode(callSuper = true, doNotUseGetters = true)
public class DefaultMap<K, V> extends HashMap<K, V> {

  private final SerializableSupplier<V> defaultFactory;

  public DefaultMap(
      final int initialCapacity,
      final float loadFactor,
      final SerializableSupplier<V> defaultFactory) {
    super(initialCapacity, loadFactor);
    this.defaultFactory = defaultFactory;
  }

  public DefaultMap(final int initialCapacity, final SerializableSupplier<V> defaultFactory) {
    super(initialCapacity);
    this.defaultFactory = defaultFactory;
  }

  public DefaultMap(final SerializableSupplier<V> defaultFactory) {
    this.defaultFactory = defaultFactory;
  }

  public DefaultMap(
      final Map<? extends K, ? extends V> m, final SerializableSupplier<V> defaultFactory) {
    super(m);
    this.defaultFactory = defaultFactory;
  }

  /**
   * Returns the value to which the specified key is mapped. If this map contains no mapping for the
   * key, then a mapping is added with the default factory value.
   *
   * <p>More formally, if this map contains a mapping from a key {@code k} to a value {@code v} such
   * that {@code (key==null ? k==null : key.equals(k))}, then this method returns {@code v};
   * otherwise it adds a new mapping with key {@code k} and value {@code defaultFactory.get()} and
   * returns this new value. (There can be at most one such mapping.)
   *
   * <p>A return value of {@code null} <i>necessarily</i> indicates that the map contains a mapping
   * for the key with the value {@code null]}.
   */
  @Override
  public V get(final Object key) {
    missing(uncheckedCastToK(key));
    return super.get(key);
  }

  @Override
  public V getOrDefault(final Object key, final V defaultValue) {
    throw new UnsupportedOperationException("DefaultMap#get function always finds a value.");
  }

  /**
   * It is safe to suppress warnings here because anyway {@link HashMap} accepts any key, even those
   * that are not of type {@code K}.
   */
  @SuppressWarnings("unchecked")
  private K uncheckedCastToK(final Object key) {
    return (K) key;
  }

  /**
   * If no mapping exists for the input parameter {@code key}, then adds a mapping with the default
   * value.
   */
  private void missing(final K key) {
    if (!this.containsKey(key)) {
      this.put(key, defaultFactory.get());
    }
  }

  @SafeVarargs
  public static <K, V> DefaultMap<K, V> of(
      final SerializableSupplier<V> defaultFactory, final Map.Entry<K, V>... entries) {
    return new DefaultMap<>(Map.ofEntries(entries), defaultFactory);
  }
}
