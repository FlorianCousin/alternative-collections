package types.main.defaultmap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import types.defaultmap.DefaultMap;
import types.defaultmap.SerializableSupplier;

import java.util.Map;

class DefaultMapEqualsTests {

  @Test
  void equalsSuper() {

    final SerializableSupplier<Boolean> defaultFactory = () -> false;
    final Map.Entry<Integer, Boolean> entryTrue = Map.entry(0, true);
    final Map.Entry<Integer, Boolean> entryFalse = Map.entry(4, false);

    final DefaultMap<Integer, Boolean> map1Entry = DefaultMap.of(defaultFactory, entryTrue);
    final DefaultMap<Integer, Boolean> map2Entries =
        DefaultMap.of(defaultFactory, entryTrue, entryFalse);

    final boolean actualEquality = map1Entry.equals(map2Entries);

    Assertions.assertFalse(actualEquality, "Equals function should call super.");
  }

  @Test
  void equalsDifferentDefaultFactory() {

    final SerializableSupplier<Boolean> defaultFactoryTrue = () -> true;
    final SerializableSupplier<Boolean> defaultFactoryFalse = () -> false;
    final Map.Entry<String, Boolean> entry = Map.entry("key", true);

    final DefaultMap<String, Boolean> mapFactoryFalse = DefaultMap.of(defaultFactoryFalse, entry);
    final DefaultMap<String, Boolean> mapFactoryTrue = DefaultMap.of(defaultFactoryTrue, entry);

    final boolean actualEquality = mapFactoryFalse.equals(mapFactoryTrue);

    Assertions.assertFalse(
        actualEquality, "Different defaultFactory should lead to different maps.");
  }

  @Test
  void equals() {

    final SerializableSupplier<Long> supplier1 = () -> 1L;

    final DefaultMap<Boolean, Long> map1 = DefaultMap.of(supplier1, Map.entry(true, 4L));
    final DefaultMap<Boolean, Long> map2 = DefaultMap.of(supplier1, Map.entry(true, 4L));

    final boolean actualEquality = map1.equals(map2);

    Assertions.assertTrue(actualEquality, "The maps should be the same.");
  }

  @Test
  void equalsNotByReference() {

    final DefaultMap<Boolean, Long> map1 = DefaultMap.of(() -> 1L, Map.entry(true, 4L));
    final DefaultMap<Boolean, Long> map2 = DefaultMap.of(() -> 1L, Map.entry(true, 4L));

    final boolean actualEquality = map1.equals(map2);

    Assertions.assertFalse(
        actualEquality,
        "The maps should not be the same because the defaultFactory have different references..");
  }
}
