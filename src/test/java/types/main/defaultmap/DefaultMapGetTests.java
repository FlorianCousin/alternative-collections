package types.main.defaultmap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import types.defaultmap.DefaultMap;

import java.util.Map;

class DefaultMapGetTests {

  @Test
  void getWithoutValue() {

    final String testKey = "testKey";
    final int expectedValue = 5;

    final DefaultMap<String, Integer> defaultMap =
        DefaultMap.of(() -> expectedValue, Map.entry("dummyKey", 7));

    Assertions.assertFalse(
        defaultMap.containsKey(testKey),
        "The tested key should not be mapped before getting the value.");
    Assertions.assertEquals(
        expectedValue,
        defaultMap.get(testKey),
        "The key with no mapping should have default value mapping.");
    Assertions.assertTrue(
        defaultMap.containsKey(testKey),
        "The tested key should be mapped after getting the value.");
  }

  @Test
  void getWithValue() {

    final String testKey = "testKey";
    final int expectedValue = 7;

    final DefaultMap<String, Integer> defaultMap =
        DefaultMap.of(() -> 5, Map.entry(testKey, expectedValue));

    Assertions.assertTrue(
        defaultMap.containsKey(testKey),
        "The tested key should be mapped before getting the value.");
    Assertions.assertEquals(
        expectedValue,
        defaultMap.get(testKey),
        "The key with mapping should have the mapped value.");
  }

  /**
   * This tests shows why it is safe to suppress warnings on {@link
   * DefaultMap#uncheckedCastToK(Object)}.
   */
  @Test
  @SuppressWarnings("SuspiciousMethodCalls")
  void getWithoutValueWrongType() {

    final int testKey = 3;
    final int expectedValue = 5;

    final DefaultMap<String, Integer> defaultMap =
        DefaultMap.of(() -> expectedValue, Map.entry("dummyKey", 7));

    Assertions.assertFalse(
        defaultMap.containsKey(testKey),
        "The tested key should not be mapped before getting the value.");
    Assertions.assertEquals(
        expectedValue,
        defaultMap.get(testKey),
        "The key with no mapping should have default value mapping.");
    Assertions.assertTrue(
        defaultMap.containsKey(testKey),
        "The tested key should be mapped after getting the value.");
  }
}
