package types.main.defaultmap;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import types.defaultmap.DefaultMap;

import java.util.Map;

class DefaultMapGetOrDefaultTests {

  @Test
  void getOrDefaultWithoutValue() {

    final String testKey = "testKey";
    final int expectedValue = 5;

    final DefaultMap<String, Integer> defaultMap =
        DefaultMap.of(() -> expectedValue, Map.entry("dummyKey", 7));

    Assertions.assertThrows(
        UnsupportedOperationException.class,
        () -> defaultMap.getOrDefault(testKey, 3),
        "getOrDefault function is not available in DefaultMap.");
  }

  @Test
  void getOrDefaultWithValue() {

    final String testKey = "testKey";
    final int expectedValue = 7;

    final DefaultMap<String, Integer> defaultMap =
        DefaultMap.of(() -> 5, Map.entry(testKey, expectedValue));

    Assertions.assertThrows(
        UnsupportedOperationException.class,
        () -> defaultMap.getOrDefault(testKey, 3),
        "getOrDefault function is not available in DefaultMap.");
  }
}
